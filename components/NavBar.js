import { Fragment, useState, useEffect, useContext } from 'react'; 
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link'; 
import UserContext from '../contexts/UserContext';

export default () => {
	const { user } = UserContext(UserContext)
	const [ isExpanded, setIsExpanded ] = useState(false)

	let RightNavOptions
	let LeftNavOptions

	if(user.email !== null){
		RightNavOptions = (
				<Fragment>
					<Link href="/logout">
						<a className="nav-link">
							Logout
						</a>
					</Link>
				</Fragment>
		)
		LeftNavOptions = ( 
				<Fragment>
					<Link href="/user/categories">
						<a className="nav-link">
							Categories
						</a>
					</Link>
					<Link href="/user/records">
						<a className="nav-link">
							Records
						</a>
					</Link>
					<Link href="/user/category-breakdown">
						<a className="nav-link">
							Breakdown
						</a>
					</Link>
				</Fragment>
		)
	}else{
		RightNavOptions = null
		LeftNavOptions = null
	}

   return(
   	<Fragment>
	   	<Navbar expanded={ isExpanded } bg="dark" expand="lg" fixed="top" variant="dark">
	   	 <Link href="/">
	   	     <a className="navbar-brand">MAASIN CITY BUDGET APP</a>
	   	 </Link>
	   	 <Navbar.Toggle onClick={ () => setIsExpanded(!isExpanded)} aria-controls="basic-navbar-nav" />
	   	 <Navbar.Collapse className="basic-navbar-nav">
	        <Nav className="mr-auto" onClick={ () => setIsExpanded(!isExpanded) }>
	        	{ RightNavOptions}
			</Nav>
			<Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) }>
	        	{ LeftNavOptions}
	        </Nav>
	   	 </Navbar.Collapse>
	   	</Navbar>
   	</Fragment>
   	)
}


<Link href="/">
<a className="nav-link">Register</a>
</Link>