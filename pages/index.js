import { useState, useContext } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import UserContext from '../contexts/UserContext';
import View from '../components/View'; 
import Swal from 'sweetalert2'
import Router from 'next/router';

export default function Home() {
 const [email, setEmail] = useState("")
 const [password, setPassword] = useState("")
 const { user, setUser } = useContext(UserContext)

	if(user.email !== null) {
		Router.push('/user/records');
	}

 const retrieveUserDetails = (accessToken) => {
	 const options = {
	 headers: { Authorization: `Bearer ${accessToken}`}
 }
 	
 	fetch('http://localhost:4000/api/users/details', options).then((response) => response.json()).then(data => {
 		setUser({ email: data.email })
		 Router.push('/user/records')

	 })
 }

 function login(e){
	e.preventDefault()
	fetch("http://localhost:4000/api/users/login", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify ({
			email: email,
			password: password
		})
	}).then(res => res.json()).then(data => {
		if(typeof accessToken !== "undefined") {
			localStorage.setItem('token', data.accessToken)
			retrieveUserDetails(data.accessToken)
			Swal.fire({
				icon: 'success',
				title: 'Successfully Logged In'
			})
			
		} else {
			Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
		}
	})
 }

  return (
      <View title={ 'Budget Tracking App' }>
         <h1>Welcome!</h1>
          <p>Login by using your Registered Email.</p>
	        <Form onSubmit={e => login(e)}>
	            <Form.Group controlId="email">
	                <Form.Label>Email:</Form.Label>
	                <Form.Control type="text" value={email} onChange={e=>setEmail(e.target.value)} required/>
	            </Form.Group>
	            <Form.Group controlId="password">
	                <Form.Label>Password:</Form.Label>
	                <Form.Control type="password" value={password} onChange={e=>setPassword(e.target.value)} required/>
	            </Form.Group>
	            <Button type="submit" variant="primary" className="btn-block mb-3">Submit</Button>
	        </Form>
      </View>
  )
}
